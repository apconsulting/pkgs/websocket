package iris

import (
	"fmt"
	"net/http"

	gorillaWs "github.com/gorilla/websocket"
	"github.com/kataras/iris/v12"
	"gitlab.com/apconsulting/pkgs/websocket"
)

var wsUpgrader *gorillaWs.Upgrader = &gorillaWs.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func SetWsUpgrader(upg *gorillaWs.Upgrader) error {
	// TODO: Verificare l'upgrader passato?
	if upg == nil {
		return fmt.Errorf("upgrader can't be nil")
	}

	wsUpgrader = upg
	return nil
}

func WsHandler(srv *websocket.WsServer) iris.Handler {
	// Endpoint che effettua l'upgrade di una connessione http(s) in ws(s), tramite l'uso del
	// package gorilla/websocket, inizializza un nuovo client e lo connette al server
	return func(ctx iris.Context) {
		ws, err := wsUpgrader.Upgrade(ctx.ResponseWriter(), ctx.Request(), nil)
		if err != nil {
			//ctx.StopWithError(iris.StatusInternalServerError, err)

			//fmt.Println("Errore in upgrade", err)
			return
		}

		client := &websocket.HttpClient{}
		client.Init(ws)

		srv.Connect(client)
	}
}
