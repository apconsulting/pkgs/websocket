package iris

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/kataras/iris/v12"
	"gitlab.com/apconsulting/pkgs/websocket"
)

func ApiHandler(srv *websocket.WsServer) iris.Handler {
	var (
		// Mappa di associazione tra UUID e ApiClient
		apiClientsMap      map[string]*websocket.ApiClient = make(map[string]*websocket.ApiClient)
		apiClientsMapMutex sync.RWMutex
	)

	// Endpoint sincrono che collega una request GET al server websocket tramite un ApiClient
	// appositamente costruito. Se la richiesta include un OpRef, attende una risposta dal
	// server, altrimenti procede senza interrompersi. Come response invia la coda dei messaggi
	// allo stato attuale, comprensiva dell'eventuale risposta attesa.
	return func(ctx iris.Context) {
		wsUuid := ctx.GetHeader(websocket.API_CLIENT_UUID_HEADER)
		// Slice dei messaggi da inviare come response
		var respQ []websocket.Message = make([]websocket.Message, 0)

		var c *websocket.ApiClient
		var exists bool = false

		if len(wsUuid) == 0 {
			// Il peer non conosce l'UUID del Client a lui associato, oppure è una nuova connessione.
			// Genero un nuovo client.

			c = &websocket.ApiClient{}

			c.Init(func(ac *websocket.ApiClient) {
				apiClientsMapMutex.Lock()
				delete(apiClientsMap, ac.GetUuid().String())
				apiClientsMapMutex.Unlock()
			})

			wsUuid = c.GetUuid().String()

			ctx.Header(websocket.API_CLIENT_UUID_HEADER, wsUuid)

			apiClientsMapMutex.Lock()
			apiClientsMap[wsUuid] = c
			apiClientsMapMutex.Unlock()
			exists = true

			// Invoco la connessione al server in una goroutine, altrimenti vado in deadlock sulla
			// StartLoop (invocata dal server). Questo non è un problema con gli HttpClient, perché
			// per loro natura devono restare bloccati (la connessione WS è full duplex, quella Api
			// ne simula solamente il comportamento)
			go srv.Connect(c)
		} else {
			// Il peer comunica un UUID ...
			ctx.Header(websocket.API_CLIENT_UUID_HEADER, wsUuid)

			apiClientsMapMutex.RLock()
			c, exists = apiClientsMap[wsUuid]
			apiClientsMapMutex.RUnlock()

			if !exists || !c.Renew() {
				// .. che io non ho registrato o non posso rinnovare. Notifico la chiusura.
				// Sarà cura del peer gestire la disconnessione e reinviare il messaggio che viene
				// droppato da questa request, in quanto non posso sapere se la logica del server
				// permette di servire il messaggio conseguentemente ad una disconnessione (banalmente,
				// potrei dover riautenticare il canale prima di servire il messaggio inviato, quindi
				// demando al peer la scelta di come procedere)

				// body, _ := json.Marshal(struct {
				// 	Uuid    string `json:"uuid"`
				// 	Message string `json:"message"`
				// }{
				// 	Uuid:    wsUuid,
				// 	Message: "expired",
				// })

				ctx.JSON([]websocket.Message{
					{
						Module: "system",
						Method: "close",
						Body:   "expired",
					},
				})
				return
			}
		}

		// A questo punto c'è il mio client, è attivo e ne conosco conosco l'UUID.
		var msg websocket.Message

		body, err := ctx.GetBody()
		if err != nil {
			ctx.StopWithError(iris.StatusNotAcceptable, err)
			return
		}

		if len(body) > 0 {
			err = json.Unmarshal(body, &msg)

			if err != nil {
				ctx.StopWithError(iris.StatusNotAcceptable, fmt.Errorf("malformed request: %w", err))
				return
			}
		}

		// Invia al server websocket il messaggio in request
		resp, err := c.FromPeer(msg)

		// Ricevuta risposta
		respQ = append(respQ, resp...)

		switch err {
		case nil, websocket.ErrApiClientTimeout:
			// Nessun errore oppure Timeout, invio la Q recuperata
			ctx.JSON(respQ)
		case websocket.ErrClosing:
			// Client in chiusura, accodo il messaggio di chiusura

			respQ = append(respQ, websocket.Message{
				Module: "system",
				Method: "close",
				Body:   "closed",
			})
			ctx.JSON(respQ)
		default:
			// Errore diverso
			ctx.StopWithError(iris.StatusInternalServerError, err)
		}

		return
	}
}
