package websocket

import (
	"fmt"
	"sync"
	// sync "github.com/sasha-s/go-deadlock"

	"github.com/google/uuid"
)

var ERR_ALREADY_SUBSCRIBED = fmt.Errorf("client already subscribed")
var ERR_CLIENT_NOT_FOUND = fmt.Errorf("client not found")

type IWsChannel interface {
	Init()

	Add(cl IWsClient) error
	Remove(cl IWsClient) (int, error)
	List() []IWsClient
}

// Assert implementation
var _ IWsChannel = &WsChannel{}

type WsChannel struct {
	clients      map[uuid.UUID]IWsClient
	clientsMutex sync.RWMutex
	Name         string // Prob inutile?
}

func (ch *WsChannel) Init() {
	ch.clients = make(map[uuid.UUID]IWsClient)
}

func (ch *WsChannel) Add(cl IWsClient) error {
	ch.clientsMutex.Lock()
	defer ch.clientsMutex.Unlock()

	if _, ok := ch.clients[cl.GetUuid()]; ok {
		return ERR_ALREADY_SUBSCRIBED
	}

	ch.clients[cl.GetUuid()] = cl

	return nil
}

func (ch *WsChannel) Remove(cl IWsClient) (int, error) {
	ch.clientsMutex.Lock()
	defer ch.clientsMutex.Unlock()

	delete(ch.clients, cl.GetUuid())

	return len(ch.clients), nil
}

func (ch *WsChannel) List() (list []IWsClient) {
	list = make([]IWsClient, 0)

	ch.clientsMutex.RLock()
	for _, v := range ch.clients {
		list = append(list, v)
	}
	ch.clientsMutex.RUnlock()

	return
}
