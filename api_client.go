package websocket

import (
	"fmt"
	"sync"
	"sync/atomic"
	// sync "github.com/sasha-s/go-deadlock"
	"time"
)

const (
	API_CLIENT_TIMEOUT         = time.Second * 10
	API_SERVER_CHANNEL_TIMEOUT = time.Second * 10
	API_CLIENT_EXPIRE          = time.Second * 60
	API_CLIENT_UUID_HEADER     = "Ap-Ws-Uuid"
)

var (
	ErrApiClientExpired     = fmt.Errorf("expired")
	ErrApiClientTimeout     = fmt.Errorf("timeout")
	ErrServerChannelTimeout = fmt.Errorf("timeout while sending on server channel")
)

type respChanWithTimer struct {
	// Ricevuta risposta
	c chan<- struct{}

	// Timeout
	t *time.Timer

	// Client in chiusura
	e chan<- struct{}
}

type ApiClient struct {
	WsClient

	// Se un messaggio in arrivo dal peer ha una OpRef settata, la registro in questa mappa
	// in modo da poter notificare l'avvenuta ricezione della risposta dal server.
	cbMap      map[string]chan<- error
	cbMapMutex sync.RWMutex

	msgQueue   []Message
	queueMutex sync.Mutex

	// Prevengo una race sull'operazione di chiusura.
	closeOnce sync.Once
	// True se il client è in chiusura.
	closing atomic.Bool
	// Callback che viene invocata alla chiusura del client.
	closeCB func(*ApiClient)
	// Timer che chiude automaticamente il client se non si fa vivo per troppo tempo.
	expireTimer *time.Timer

	// Accesso esclusivo al canale verso il server.
	srvMutex sync.Mutex
}

var _ IWsClientComplete = &ApiClient{}

func (c *ApiClient) StartLoop() <-chan ChannelPayload {
	//go c.listenLoop()
	return c.SrvChannel
}

// @ ---> [  ] From server to peer
func (c *ApiClient) Send(m Message) error {
	if c.closing.Load() {
		return ErrClosing
	}

	// Accoda il messaggio
	c.queueMutex.Lock()
	c.msgQueue = append(c.msgQueue, m)
	c.queueMutex.Unlock()

	// Verifico se per questo messaggio esiste un canale di notifica e nel caso invio il segnale
	c.cbMapMutex.RLock()
	if ch, ok := c.cbMap[m.OpRef]; ok && ch != nil {
		ch <- nil
	}
	c.cbMapMutex.RUnlock()

	return nil
}

// Inizializza il Client. Accetta in ingresso una Callback che verrà invocata al momento della
// chiusura del client.
func (c *ApiClient) Init(closeCB func(*ApiClient)) {
	c.cbMap = make(map[string]chan<- error)
	c.msgQueue = make([]Message, 0)

	// Termino il client se non ricevo comunicazioni entro un determinato intervallo di tempo
	c.expireTimer = time.AfterFunc(API_CLIENT_EXPIRE, func() {
		c.Close()
	})

	c.closeCB = closeCB

	c.WsClient.Init()
}

// Rinnovo la durata del timer che mantiene vivo il client. Ritorna false se il timer è già
// scaduto e true se è stato rinnovato correttamente
func (c *ApiClient) Renew() bool {
	if c.closing.Load() {
		return false
	}

	if !c.expireTimer.Stop() {
		return false
	}

	c.expireTimer.Reset(API_CLIENT_EXPIRE)
	return true
}

// Invia un messaggio al server @ <--- [  ], se il messaggio non è vuoto. Un messaggio è
// considerato vuoto se la proprietà Method è vuota.
// Se il messaggio contiene un OpRef resta in attesa di risposta da parte del server prima
// di ritornare. Il valore di ritorno è l'intera coda dei messaggi in attesa, comprensiva
// dell'eventuale risposta ad m.
func (c *ApiClient) FromPeer(m Message) (q []Message, err error) {
	if len(m.Method) == 0 {
		// Se il messaggio m è vuoto (il Method è sempre obbligatorio, se manca considero
		// il messaggio come vuoto), recupero la coda e la ritorno senza fare altro.
		q = c.GetQueue()
		return
	}

	if len(m.OpRef) > 0 {
		// Se il messaggio m contiene un OpRef
		c.cbMapMutex.RLock()
		_, ok := c.cbMap[m.OpRef]
		c.cbMapMutex.RUnlock()

		if ok {
			err = fmt.Errorf("opRef already registered")
			return
		}

		defer func() {
			// Piggybacking
			q = c.GetQueue()
		}()

		// Registro il canale associandolo all'OpRef di questo messaggio, cosi da poter essere
		// notificato della risposta una volta che il server me la invia.
		respChan := make(chan error, 2)
		c.cbMapMutex.Lock()
		c.cbMap[m.OpRef] = respChan
		c.cbMapMutex.Unlock()

		defer func() {
			// Comunque vada, elimino il canale dalla mappa ad operazione terminata.
			c.cbMapMutex.Lock()
			delete(c.cbMap, m.OpRef)
			c.cbMapMutex.Unlock()
		}()

		// Invia al server
		if err = c.sendToServer(ChannelPayload{
			Message: m,
		}); err != nil {
			return
		}

		// A questo punto l'invio al server è avvenuto. Setto un timeout sulla ricezione della risposta
		t := time.NewTimer(API_CLIENT_TIMEOUT)
		defer t.Stop()

		// Blocca fino alla risposta, al timeout o alla chiusura del client
		select {
		case <-t.C:
			err = ErrApiClientTimeout
		case err = <-respChan:
			// Ritorno l'eventuale errore ricevuto dal canale (per ora solamente ErrClosing o nil)
		}
	} else {
		// Nessun OpRef specificato, recupero la queue senza registrare callback e invio il
		// messaggio al server
		q = c.GetQueue()
		err = c.sendToServer(ChannelPayload{
			Message: m,
		})
	}

	return
}

// Recupera l'attuale queue dei messaggi e contestualmente la sostituisce con una slice vuota.
// Il valore di ritorno è la queue recuperata.
func (c *ApiClient) GetQueue() (q []Message) {
	c.queueMutex.Lock()
	defer c.queueMutex.Unlock()

	q = c.msgQueue
	c.msgQueue = make([]Message, 0)

	return
}

// Invia un payload al server, verificando che il canale sia ancora aperto prima di procedere,
// evitando così una race condition tra le varie goroutine che usano il canale (particolarmente
// grave il caso di invio su canale chiuso)
func (c *ApiClient) sendToServer(m ChannelPayload) (err error) {
	if c.closing.Load() {
		err = ErrClosing
		return
	}

	defer func() {
		if x := recover(); x != nil {
			err, ok := x.(error)
			if ok && err.Error() == "send on closed channel" {
				fmt.Printf("PANIC RECOVERED: SrvChannel closed for ApiClient [%s]\n", c.GetUuid().String())
			} else {
				panic(x)
			}
		}
	}()

	// Setto un timeout sull'operazione di invio al server del messaggio.
	t := time.NewTimer(API_SERVER_CHANNEL_TIMEOUT)
	defer t.Stop()

	c.srvMutex.Lock()
	defer c.srvMutex.Unlock()

	select {
	case c.SrvChannel <- m:
	case <-t.C:
		err = ErrServerChannelTimeout
	}

	return
}

func (c *ApiClient) Close() {
	c.closeOnce.Do(func() {
		c.closing.Store(true)

		c.expireTimer.Stop()

		// Chiudo il canale verso il server per notificarlo
		c.srvMutex.Lock()
		close(c.SrvChannel)
		c.srvMutex.Unlock()

		// Svincolo le goroutine in attesa di risposta, notificando l'evento di chiusura
		c.cbMapMutex.Lock()
		for _, cb := range c.cbMap {
			if cb != nil {
				cb <- ErrClosing
			}
		}
		c.cbMap = make(map[string]chan<- error)
		c.cbMapMutex.Unlock()

		c.queueMutex.Lock()
		c.msgQueue = make([]Message, 0)
		c.queueMutex.Unlock()

		// Notifico chi ha creato il client della sua chiusura (ie. l'endpoint API)
		c.closeCB(c)
	})
}
