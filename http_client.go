package websocket

import (
	"encoding/json"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	gorillaWs "github.com/gorilla/websocket"
)

// Assert implementation
var _ IWsClient = &HttpClient{}
var ErrClosing = fmt.Errorf("client is closing")

const (
	PING_PERIOD    = 30 * time.Second
	PING_TIMEOUT   = 5 * time.Second
	WRITE_DEADLINE = 5 * time.Second
)

// Implementazione di IWsClientComplete che usa il protocollo HTTP per la connessione
// sfruttando il pacchetto github.com/gorilla/websocket
type HttpClient struct {
	WsClient

	conn *gorillaWs.Conn

	// True se il client è in chiusura.
	closing atomic.Bool

	// Accesso esclusivo alla sottostante connessione gorillaWs
	connMutex sync.Mutex

	// Accesso esclusivo al canale verso il server. Evita una closed channel panic se la goroutine
	// listenLoop cerca di inviare sul canale quando questo è stato chiuso dalla goroutine che gestisce
	// il ping
	srvMutex sync.Mutex

	// Canale di controllo per terminare la goroutine del ping
	stopPingChan chan struct{}

	// Prevengo una race sull'operazione di chiusura
	closeOnce sync.Once
}

// Assert implementation
var _ IWsClientComplete = &HttpClient{}

// Inizializza il client con la connessione GorillaWS fornita
func (c *HttpClient) Init(conn *gorillaWs.Conn) {
	c.conn = conn
	c.stopPingChan = make(chan struct{})
	c.closing.Store(false)

	go c.pingLoop()

	c.WsClient.Init()
}

// Invia un messaggio dal server al client @ --> [ ]
func (c *HttpClient) Send(m Message) error {
	// Preveniamo qualche casino...
	if c.closing.Load() {
		return ErrClosing
	}

	c.connMutex.Lock()
	defer c.connMutex.Unlock()

	if err := c.conn.WriteJSON(m); err != nil {
		return err
	}

	return nil
}

// Avvia main loop. Ritorna il canale utilizzato per la comunicazione verso il server
func (c *HttpClient) StartLoop() <-chan ChannelPayload {
	go c.listenLoop()
	return c.SrvChannel
}

// Resta in ascolto di messaggi provenienti dalla connessione GorillaWS. Alla chiusura della connessione
// chiude il canale di comunicazione con il server.
func (c *HttpClient) listenLoop() {
	defer func() {
		// Loop terminato, chiudo il canale per avvertire il server
		c.Close()
	}()

	for {
		var pl ChannelPayload
		_, p, err := c.conn.ReadMessage()

		if err != nil {
			if !gorillaWs.IsCloseError(err, 1000) {
				pl.Error = err
				c.sendToServer(pl)
			}

			break
		}

		msg := Message{}
		err = json.Unmarshal(p, &msg)

		if err != nil {
			pl.Error = err

			// invia l'errore al server
			c.sendToServer(pl)

			continue
		}

		pl.Error = nil
		pl.Message = msg

		// Invia messaggio al server
		c.sendToServer(pl)
	}
}

// Goroutine che invia periodicamente un frame di controllo "ping" sul canale e attende
// il relativo "pong". Nel caso sopravvenissero errori, o se il frame "pong" non dovesse
// giungere in tempo, termina il Client.
func (c *HttpClient) pingLoop() {
	ticker := time.NewTicker(PING_PERIOD)
	pongChan := make(chan struct{})

	// Non so se ha molto senso, ma meglio prevenire che curare
	oldPongHandler := c.conn.PongHandler()

	c.conn.SetPongHandler(func(appData string) error {
		pongChan <- struct{}{}
		return nil
	})

	defer func() {
		ticker.Stop()
		c.conn.SetPongHandler(oldPongHandler)
	}()

	for {
		select {
		case <-ticker.C:
			c.connMutex.Lock()
			err := c.conn.WriteControl(gorillaWs.PingMessage, nil, time.Now().Add(WRITE_DEADLINE))
			c.connMutex.Unlock()

			if err != nil {
				// Errore durante l'invio del ping di controllo. Notifico il server...
				c.sendToServer(ChannelPayload{
					Error: err,
				})

				// ...e termino il client.
				c.Close()
				return
			}

			select {
			case <-time.After(PING_TIMEOUT):
				// In caso di timeout, notifico e termino il client.
				c.sendToServer(ChannelPayload{
					Error: fmt.Errorf("ping timeout"),
				})

				c.Close()
				return
			case <-pongChan:
				// Pong ricevuto in tempo, nulla da fare.
			}
		case _, ok := <-c.stopPingChan:
			if !ok {
				return
			}
		}
	}
}

// Termina forzatamente il client
func (c *HttpClient) Close() {
	c.closeOnce.Do(func() {
		c.closing.Store(true)

		c.srvMutex.Lock()
		// Chiudo la goroutine che invia i frame di controllo "ping"
		close(c.stopPingChan)
		// Chiudo il canale verso il server per notificarlo
		close(c.SrvChannel)
		c.srvMutex.Unlock()

		// Invio un frame di controllo "close" al peer remoto, per avvertirlo della chiusura della
		// connessione. La conn.Close() non lo fa automaticamente.
		c.conn.WriteControl(gorillaWs.CloseMessage, nil, time.Now().Add(WRITE_DEADLINE))

		// Chiudo la connessione gorillaWs
		c.conn.Close()
	})
}

// Invia un payload al server, verificando che il canale sia ancora aperto prima di procedere,
// evitando così una race condition tra le varie goroutine che usano il canale (nello specifico,
// la pingLoop e la listenLoop)
func (c *HttpClient) sendToServer(m ChannelPayload) (err error) {
	if c.closing.Load() {
		err = fmt.Errorf("client closing")
		return
	}

	defer func() {
		if x := recover(); x != nil {
			err, ok := x.(error)
			if ok && err.Error() == "send on closed channel" {
				fmt.Printf("PANIC RECOVERED: SrvChannel closed for HttpClient [%s]\n", c.GetUuid().String())
			} else {
				panic(x)
			}
		}
	}()

	c.srvMutex.Lock()
	c.SrvChannel <- m
	c.srvMutex.Unlock()

	return
}
