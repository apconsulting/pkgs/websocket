package websocket

import (
	"fmt"
	"log"
	"os"
	"sync"
	// sync "github.com/sasha-s/go-deadlock"

	"github.com/google/uuid"
)

var ERR_MODULE_ALREADY_REGISTERED = fmt.Errorf("a module with the same name is already registered")
var ERR_MODULE_NOT_FOUND = fmt.Errorf("module not found")
var ERR_MODULE_NOT_SPECIFIED = fmt.Errorf("module not specified")
var ERR_CHANNEL_NOT_FOUND = fmt.Errorf("channel not found")

// Interfaccia del server, utilizzata dai moduli
type IWsServer interface {
	// Esegue la funzione "f" per ciascun client connesso.
	ForEachClient(f func(IWsClient) error) error

	// Invia un messaggio a tutti i client che soddisfano il predicato. @ ---> [ ]
	// La funzione predicato accetta un client in ingresso e ritorna un bool che indica se
	// il client deve ricevere il messaggio oppure no.
	BroadcastWithPredicate(m Message, p func(IWsClient) bool) error

	// Chiude forzatamente la connessione di un client
	CloseClient(c IWsClient)

	// Rimuove l'autorizzazione di un client. Metodo disponibile ai moduli
	ForbidClient(c IWsClient) error

	// Permette di settare un logger diverso per la verbose mode
	SetLogger(*log.Logger)
}

// Assert implementation
var _ IWsServer = &WsServer{}

// Type WsServer
type WsServer struct {
	modules  map[string]IWsModule
	clients  map[uuid.UUID]IWsClientComplete
	channels map[string]IWsChannel

	modulesLock  sync.RWMutex
	clientsLock  sync.RWMutex
	channelsLock sync.RWMutex

	verbose bool
	logger  *log.Logger

	Name string
}

// Crea ed inizializza una nuova istanza di WsServer
func New(name string) *WsServer {
	srv := WsServer{}
	srv.modules = make(map[string]IWsModule)
	srv.clients = make(map[uuid.UUID]IWsClientComplete)
	srv.channels = make(map[string]IWsChannel)

	prefix := "[WS] "
	if name != "" {
		prefix = fmt.Sprintf("[WS-%s] ", name)
	}

	// Default logger su os.Stdout
	srv.logger = log.New(os.Stdout, prefix, log.Ldate|log.Lmicroseconds)

	return &srv
}

func (s *WsServer) SetLogger(logger *log.Logger) {
	s.log(": INFO  [SWITCHING LOGGER]")
	s.logger = logger
	s.log(": INFO  [NEW LOGGER ACQUIRED]")
}

// Imposta o toglie la verbose mode del server.
func (s *WsServer) SetVerboseMode(v bool) {
	if v {
		s.verbose = true
		s.log(": INFO  [ENTERING VERBOSE MODE]")
	} else {
		s.verbose = false
		s.log(": INFO  [EXITING VERBOSE MODE]")
	}
}

// Registra un modulo nel server. Recupera una nuova sessione per tutti i client connessi.
// Ritorna ERR_MODULE_ALREADY_REGISTERED se un modulo con lo stesso nome è già stato registrato
func (s *WsServer) Register(m IWsModule, n string) error {
	m.Init(n, s)

	s.modulesLock.RLock()
	_, ok := s.modules[m.GetName()]
	s.modulesLock.RUnlock()

	if ok {
		return ERR_MODULE_ALREADY_REGISTERED
	}

	s.modulesLock.Lock()
	s.modules[m.GetName()] = m
	s.modulesLock.Unlock()

	s.clientsLock.RLock()
	for _, c := range s.clients {
		s.clientsLock.RUnlock()

		newSess := m.ClientConnect(c)
		if newSess != nil {
			c.session().set(m.GetName(), newSess)
		}

		s.clientsLock.RLock()
	}
	s.clientsLock.RUnlock()

	return nil
}

// Toglie la registrazione del modulo dal server. Cancella la sessione relativa al modulo
// da tutti i client. Ritorna ERR_MODULE_NOT_FOUND se il modulo non è presente.
func (s *WsServer) Unregister(mod IWsModule) error {
	s.modulesLock.RLock()
	_, ok := s.modules[mod.GetName()]
	s.modulesLock.RUnlock()

	if !ok {
		return ERR_MODULE_NOT_FOUND
	}

	mod.Cleanup()

	s.modulesLock.Lock()
	delete(s.modules, mod.GetName())
	s.modulesLock.Unlock()

	s.clientsLock.RLock()
	for _, c := range s.clients {
		// s.clientsLock.RUnlock()

		c.session().remove(mod.GetName())

		// s.clientsLock.RLock()
	}
	s.clientsLock.RUnlock()

	return nil
}

// Logga un messaggio sul logger impostato se la verbose mode è attivata
func (s *WsServer) log(msg string, params ...any) {
	m := fmt.Sprintf(msg, params...)

	if len(m) > 450 {
		m = m[0:450] + " ..."
	}

	if s.verbose {
		s.logger.Println(m)
	}
}

// Metodo che gestisce un messaggio in arrivo. Chiama HandleMessage del modulo relativo,
// passando metodo, body del messaggio e il client che l'ha inviato. Se il metodo ritorna
// una nuova sessione, la sostituisce a quella del client.
// Ritorna ERR_MODULE_NOT_SPECIFIED se il messaggio non specifica un modulo.
// Ritorna ERR_MODULE_NOT_FOUND se il messaggio specifica un modulo non valido
func (s *WsServer) Handle(c IWsClientComplete, msg Message) error {
	if msg.Module == "" {
		return ERR_MODULE_NOT_SPECIFIED
	}

	s.modulesLock.RLock()
	module, ok := s.modules[msg.Module]
	s.modulesLock.RUnlock()

	if !ok {
		return ERR_MODULE_NOT_FOUND
	}

	opRef := ""
	if msg.OpRef != "" {
		opRef = ".\"" + msg.OpRef + "\""
	}

	s.log("@ <---- [%s]: [%s] \"%s\"%s %s", c.GetUuid().String(), msg.Module, msg.Method, opRef, msg.Body)

	if newSess := module.HandleMessage(msg.Method, msg.Body, msg.OpRef, c, c.GetModuleSession(module.GetName())); newSess != nil {
		c.session().set(msg.Module, newSess)
	}

	return nil
}

// Connette un nuovo client al server, lo inizializza e ne avvia il Main Loop, restando in attesa
func (s *WsServer) Connect(c IWsClientComplete) {
	// Registra il client nella mappa dei clients
	s.clientsLock.Lock()
	s.clients[c.GetUuid()] = c
	s.clientsLock.Unlock()

	s.log("@ <-+-- [%s]: [NEW CLIENT]", c.GetUuid().String())

	// Notifica i moduli dell'avvenuta connessione di un Client e inizializza la sessione
	s.modulesLock.RLock()
	for _, m := range s.modules {
		newSess := m.ClientConnect(c)
		if newSess != nil {
			c.session().set(m.GetName(), newSess)
		}
	}
	s.modulesLock.RUnlock()

	// Resta in ascolto per i messaggi in arrivo dal client @ <-- [ ]
	// Una volta che il canale di comunicazione viene chiuso elimina il client
	ch := c.StartLoop()

	for msg := range ch {
		if msg.Error != nil {
			s.log("@ <-X-- [%s]: [ERROR] %s", c.GetUuid().String(), msg.Error.Error())
		} else {
			if err := s.Handle(c, msg.Message); err != nil {
				s.log("@ <-X-- [%s]: [ERROR] %s %v", c.GetUuid().String(), err.Error(), msg)
				c.Send(Message{
					Module: "system",
					Method: "error",
					Body:   err.Error(),
				})
			}
		}
	}

	// CANALE CHIUSO
	s.log("@ <-|-- [%s]: [CHANNEL CLOSED]", c.GetUuid().String())

	// Rimuove il client da tutti i canali
	s.channelsLock.Lock()
	for k, ch := range s.channels {
		i, _ := ch.Remove(c)
		if i == 0 {
			delete(s.channels, k)
		}
	}
	s.channelsLock.Unlock()

	// Cancella il client dalla mappa
	s.clientsLock.Lock()
	delete(s.clients, c.GetUuid())
	s.clientsLock.Unlock()

	// Notifica i moduli dell'avvenuta disconnessione
	s.modulesLock.RLock()
	for _, m := range s.modules {
		m.ClientDisconnect(c)
	}
	s.modulesLock.RUnlock()
}

// Esegue la funzione "f" per ciascun client connesso. Metodo disponibile ai moduli
func (s *WsServer) ForEachClient(f func(IWsClient) error) error {
	s.clientsLock.RLock()
	defer s.clientsLock.RUnlock()

	for _, c := range s.clients {
		// s.clientsLock.RUnlock()

		if err := f(c); err != nil {
			return err
		}

		// s.clientsLock.RLock()
	}

	return nil
}

// Sottoscrive un client ad un canale specifico, creandolo se non dovesse esistere
func (s *WsServer) Subscribe(c IWsClient, channelName string) error {
	s.channelsLock.Lock()
	defer s.channelsLock.Unlock()

	ch, ok := s.channels[channelName]

	if !ok {
		ch = &WsChannel{
			Name: channelName,
		}
		ch.Init()

		s.channels[channelName] = ch
	}

	return ch.Add(c)
}

// Annulla l'iscrizione di un client ad uno specifico canale, eliminando il canale se dovesse essere rimasto vuoto
func (s *WsServer) Unsubscribe(c IWsClient, channelName string) error {
	s.channelsLock.Lock()
	defer s.channelsLock.Unlock()

	ch, ok := s.channels[channelName]
	if !ok {
		return nil
	}

	length, err := ch.Remove(c)
	if err != nil {
		return err
	}

	if length == 0 {
		delete(s.channels, channelName)
	}

	return nil
}

func (s *WsServer) ListChannels() []string {
	var list []string = make([]string, 0)

	s.channelsLock.RLock()
	for k := range s.channels {
		list = append(list, k)
	}
	s.channelsLock.RUnlock()

	return list
}

// Invia un messaggio a tutti i client che soddisfano il predicato. @ ---> [ ]
// La funzione predicato accetta un client come parametro e ritorna un bool che indica se
// il client deve ricevere il messaggio oppure no. Se predicate == nil, l'invio viene
// effettuato a tutti i client.
func (s *WsServer) BroadcastWithPredicate(m Message, predicate func(IWsClient) bool) error {
	var errors []error
	s.ForEachClient(func(c IWsClient) error {
		if predicate != nil && !predicate(c) {
			return nil
		}

		err := s.SendToClient(c, m)
		if err != nil {
			errors = append(errors, err)
		}

		return nil
	})

	var returnError error

	for _, err := range errors {
		returnError = fmt.Errorf("%s - %s", returnError, err)
	}

	return returnError
}

// Invia un messaggio a tutti i client autenticati. @ ---> [ ]
func (s *WsServer) BroadcastToAuth(m Message) error {
	return s.BroadcastWithPredicate(m, func(c IWsClient) bool { return c.AuthInfo().Authenticated })
}

// Invia un messagio a tutti i client connessi indistintamente. @ ---> [ ]
func (s *WsServer) BroadcastToAll(m Message) error {
	return s.BroadcastWithPredicate(m, nil)
}

// Invia un messaggio a tutti i client sottoscritti ad uno specifico canale che soddisfano l'eventuale
// predicato. Se predicate == nil, invia a tutti i client sottoscritti. @ ---> [ ]
func (s *WsServer) BroadcastToChannel(m Message, chName string, predicate func(c IWsClient) bool) error {
	s.channelsLock.RLock()
	defer s.channelsLock.RUnlock()

	ch, ok := s.channels[chName]

	if !ok {
		return ERR_CHANNEL_NOT_FOUND
	}

	for _, cl := range ch.List() {
		if predicate == nil || predicate(cl) {
			if err := s.SendToClient(cl, m); err != nil {
				s.log("@ --!-> [%s]: [WARN] Error in SendToClient while broadcasting to channel: %s %v", cl.GetUuid().String(), err.Error(), m)
			}
		}
	}

	return nil
}

// Invia un messaggio ad un client specifico. Metodo disponibile ai moduli. @ ---> [ ]
func (s *WsServer) SendToClient(c IWsClient, m Message) error {
	cl := c.(IWsClientComplete)

	if err := cl.Send(m); err != nil {
		s.log("@ --X-> [%s]: [ERROR] SendToClient: %s | %v", cl.GetUuid().String(), err.Error(), m)
		//s.CloseClient(c)
		return err
	}

	opRef := ""
	if m.OpRef != "" {
		opRef = fmt.Sprintf(".\"%s\"", m.OpRef)
	}

	s.log("@ ----> [%s]: [%s] \"%s\"%s - %s", cl.GetUuid().String(), m.Module, m.Method, opRef, m.Body)
	return nil
}

// Invia un messaggio ad un client identificato tramite il proprio UUID. Metodo disponibile ai moduli. @ ---> [ ]
func (s *WsServer) SendToUuid(strUuid string, m Message) error {
	id, err := uuid.Parse(strUuid)
	if err != nil {
		return err
	}

	s.clientsLock.RLock()
	c, ok := s.clients[id]
	s.clientsLock.RUnlock()

	if !ok {
		s.log("@ --!-> [%s]: [WARN] SendToUuid: Client not found with given Uuid String: %s", id.String(), strUuid)
		return nil
	}

	return s.SendToClient(c, m)
}

// Chiude forzatamente la connessione di un client. Metodo disponibile ai moduli.
func (s *WsServer) CloseClient(c IWsClient) {
	cl := c.(IWsClientComplete)

	// if err := cl.Close(); err != nil {
	// 	s.log("@ --X-> [%s]: [ERROR] CloseClient: %s", c.GetUuid().String(), err.Error())
	// 	return err
	// }
	cl.Close()

	s.log("@ --|-> [%s]: [CLIENT FORCIBLY DISCONNECTED]", c.GetUuid().String())
}

// Rimuove l'autorizzazione di un client. Metodo disponibile ai moduli
func (s *WsServer) ForbidClient(c IWsClient) error {
	c.Forbid()

	s.log(": INFO  [%s]: [CLIENT FORBIDDEN]", c.GetUuid().String())
	return nil
}
