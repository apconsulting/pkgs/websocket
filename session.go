package websocket

import (
	"sync"
	// sync "github.com/sasha-s/go-deadlock"
)

//var ERR_SESSION_NOT_FOUND = fmt.Errorf("session not found")

// Type SessionMap
type SessionMap struct {
	modSessMap map[string]any

	mutexLock sync.RWMutex

	//Conn ConnectionInfo
}

func (s *SessionMap) Init() {
	s.modSessMap = make(map[string]any)
}

func (s *SessionMap) Get(m string) any {
	s.mutexLock.RLock()
	defer s.mutexLock.RUnlock()

	return s.modSessMap[m]
}

func (s *SessionMap) set(m string, ctx any) {
	s.mutexLock.Lock()
	s.modSessMap[m] = ctx
	s.mutexLock.Unlock()
}

func (s *SessionMap) remove(m string) {
	s.mutexLock.Lock()
	delete(s.modSessMap, m)
	s.mutexLock.Unlock()
}
