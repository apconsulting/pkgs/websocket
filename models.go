package websocket

// Type AuthInfo
type AuthInfo struct {
	Authenticated bool
	Username      string
	UserID        int
	//AuthDate      time.Time
}

// Type Message
type Message struct {
	Module string
	Method string
	OpRef  string `json:",omitempty"`
	Body   string `json:",omitempty"`
}

type ChannelPayload struct {
	Message Message
	Error   error
}
