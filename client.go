package websocket

import (
	"sync"
	// sync "github.com/sasha-s/go-deadlock"
	"time"

	"github.com/google/uuid"
)

// Interfaccia completa del client, con i metodi disponibili al server.
type IWsClientComplete interface {
	// Include IWsClient
	IWsClient

	// Recupera l'intera sessione del client, per referenza, così da poterla modificare.
	// Accessibile solo dal server
	session() *SessionMap

	// Avvia il main loop del client che resta in attesa di messaggi. Ritorna un canale
	// per la comunicazione con il server. Il client notifica la propria disconnessione
	// al server chiudendo il canale
	StartLoop() <-chan ChannelPayload

	// Ritorna il canale di comunicazione col server. Il client utilizza questo canale per
	// inviare i messaggi ricevuti al server. In caso di disconnessione, Il client notifica
	// il server chiudendo il canale
	// GetSrvChannel() <-chan ChannelPayload

	// Invia un messaggio dal server al client @ --> [ ]
	Send(Message) error

	// Forza la disconnessione del client da parte del server
	Close()
}

// Interfaccia ridotta del Client, che espone solamente i metodi a cui hanno acesso i moduli.
type IWsClient interface {
	// Recupera una copia della sessione di uno specifico modulo
	GetModuleSession(string) any

	// Recupera l'UUID del client
	GetUuid() uuid.UUID

	// Recupera le informazioni di autenticazione del client
	AuthInfo() AuthInfo

	// Recupera data/ora di login del client
	AuthDate() time.Time

	// Autentica il client, settando le informazioni di autenticazione. I parametri sono
	// NomeUtente string e UserID int. Il flag Authenticated è settato automaticamente a
	// true
	Authenticate(string, int)

	// Svuota le informazioni di autenticazione del client
	Forbid()
}

// Assert implementation
var _ IWsClient = &WsClient{}

type WsClient struct {
	//IWsClientComplete

	uuid uuid.UUID
	sess *SessionMap
	auth AuthInfo

	authDate time.Time

	SrvChannel chan ChannelPayload

	authMutex sync.RWMutex
}

// Recupera l'intera sessione del client, per referenza
func (c *WsClient) session() *SessionMap {
	return c.sess
}

// Recupera le informazioni di autenticazione
func (c *WsClient) AuthInfo() AuthInfo {
	c.authMutex.RLock()
	defer c.authMutex.RUnlock()
	return c.auth
}

// Recupera data/ora di login del client
func (c *WsClient) AuthDate() time.Time {
	c.authMutex.RLock()
	defer c.authMutex.RUnlock()
	return c.authDate
}

// Ritorna l'identificativo univoco del client
func (c *WsClient) GetUuid() uuid.UUID {
	return c.uuid
}

// Recupera la sessione relativa ad uno specifico modulo
func (c *WsClient) GetModuleSession(m string) any {
	return c.session().Get(m)
}

// Inizializza il client
func (c *WsClient) Init() {
	s := &SessionMap{}
	s.Init()

	c.sess = s
	c.uuid = uuid.New()
	c.auth.Authenticated = false

	c.SrvChannel = make(chan ChannelPayload)
}

// Autentica un client con Username "n" e UserID "id"
func (c *WsClient) Authenticate(n string, id int) {
	c.authMutex.Lock()
	c.auth.Username = n
	c.auth.UserID = id
	c.auth.Authenticated = true
	c.authDate = time.Now()
	c.authMutex.Unlock()
}

// Rimuove le informazioni di autenticazione del client
func (c *WsClient) Forbid() {
	c.authMutex.Lock()
	c.auth = AuthInfo{}
	c.authMutex.Unlock()
}

// // Ritorna il canale di comunicazione client/server
// func (c *WsClient) GetSrvChannel() <-chan ChannelPayload {
// 	return c.SrvChannel
// }
