package websocket

// Interface
type IWsModule interface {
	// Inizializza il modulo. Setta nome e WsServer passati come parametri
	Init(n string, s *WsServer)

	// Metodo che viene invocato quando il modulo viene de-registrato dal server
	Cleanup()

	// Metodo per la gestione del messaggio in ingresso. I parametri sono il body del
	// messaggio e il client che l'ha inviato. Il valore di ritorno è la nuova sessione
	// modificata dopo la chiamata, oppure nil.
	HandleMessage(method string, body string, opRef string, c IWsClient, sess any) any

	// Gestisce la connessione di un nuovo client. Il valore di ritorno è una nuova sessione.
	ClientConnect(c IWsClient) any

	// Gestisce la disconnessione di un client dal server.
	ClientDisconnect(IWsClient)

	// Ritorna il nome del modulo.
	GetName() string

	// Formatta un messaggio e lo invia al client "c"
	Send(c IWsClient, method string, body string, opRef string) error
}

type WsModule struct {
	Name string
	Srv  *WsServer
}

// Inizializza il modulo. Setta nome e WsServer passati come parametri
func (m *WsModule) Init(n string, s *WsServer) {
	m.Name = n
	m.Srv = s
}

func (m *WsModule) Cleanup() {}

// Ritorna il nome del modulo
func (m WsModule) GetName() string {
	return m.Name
}

// Formatta un messaggio e lo invia al client "c"
func (m WsModule) Send(c IWsClient, method string, body string, opRef string) error {
	return m.Srv.SendToClient(c, Message{
		Module: m.GetName(),
		Method: method,
		OpRef:  opRef,
		Body:   body,
	})
}

// Gestisce la connessione di un nuovo client. Il valore di ritorno è una nuova sessione vuota.
func (m WsModule) ClientConnect(c IWsClient) any {
	return nil
}

// Gestisce la disconnessione di un client dal server.
func (m WsModule) ClientDisconnect(c IWsClient) {}
